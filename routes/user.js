const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

// USER ROUTES
// Register user
router.post('/', (req, res) => {
    UserController.register(req.body)
    .then(result => res.send(result))
})

// Login user
router.post('/login', (req, res) => {
    UserController.login(req.body)
    .then(result => res.send(result))
})

// Get user details from token
router.get('/details', auth.verify, (req, res) => {
    UserController.getDetails({userId:auth.decode(req.headers.authorization).id})
    .then(result => res.send(result))
})

// Get user details
router.get('/:id', (req, res) => {
    UserController.getDetails({userId:req.params.id})
    .then(result => res.send(result))
})

// Get all users
router.get('/', (req, res) => {
    UserController.getAllIds().then(result => res.send(result))
})

// Update user
router.put('/:id', auth.verify, (req, res) => {
    const arg = {
        updaterId: auth.decode(req.headers.authorization).id,
        userId: req.params.id,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        age: req.body.age,
        birth_date: req.body.birth_date,
        email: req.body.email,
        password: req.body.password,
        job_title: req.body.job_title,
        access_level_id: req.body.access_level_id
    }
    UserController.updateUser(arg)
    .then(result => res.send(result))
})

// Delete user
router.delete('/:id', auth.verify, (req, res) => {
    const arg = {
        updaterId: auth.decode(req.headers.authorization).id,
        userId: req.params.id
    }
    UserController.deleteUser(arg)
    .then(result => res.send(result))
})

module.exports = router