const mongoose = require('mongoose')

const accessLevelSchema = new mongoose.Schema({
    access_level_id: {
        type: String,
        required: [true, 'Access Level Id is required']
    },
    description: {
        type: String,
        required: [true, 'Description is required']
    }
})

module.exports = mongoose.model('access_levels', accessLevelSchema)