const User = require('../models/user')
const auth = require('../auth')
const bcrypt = require('bcrypt')
require('dotenv').config()

// USER CONTROLLERS
// Register user
module.exports.register = (params) => {
    const user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        age: params.age,
        birth_date: params.birth_date,
        email: params.email,
        password: bcrypt.hashSync(params.password, 10),
        job_title: params.job_title,
        access_level_id: params.access_level_id
    })

    return user.save()
    .then((user, err) => {
        if (user !== null) {
            return err ? false : true
        }
    })
}

// Login user
module.exports.login = (params) => {
    return User.findOne({ email: params.email })
    .then((user) => {
        if (user === null) {
            return false
        }

        const passwordCheck = bcrypt.compareSync(params.password, user.password)

        if (passwordCheck) {
            return {
                accessToken: auth.createAccessToken(user.toObject())
            }
        } else {
            return false
        }
    })
}

// Get user details
module.exports.getDetails = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if (err) return false
        user.password = undefined
        return user
    })
}

// Get all users
module.exports.getAllIds = () => {
    return User.find()
    .then((users, err) => {
		if(err) return false
		const ids = users.map(user => user)
		return ids
	})
}

// Update user
module.exports.updateUser = (params) => {
    return User.findById(params.updaterId)
    .then((user, err) => {
        if (err) {
            return false
        } else if (user.access_level_id === "Super User") {
            return User.findById(params.userId)
            .then((user, err) => {
                if (err) return false
                user.firstName = params.firstName
                user.lastName = params.lastName
                user.age = params.age
                user.birth_date = params.birth_date
                user.email = params.email
                user.password = bcrypt.hashSync(params.password, 10)
                user.job_title = params.job_title
                user.access_level_id = params.access_level_id
                return user.save()
                .then((updatedUser, err) => {
                    return (err) ? false : true
                })
            })
        } else {
            return false
        }
    })
}

// Delete user category
module.exports.deleteUser = (params) => {
    return User.findById(params.updaterId)
    .then((user, err) => {
        if (err) {
            return false
        } else if (user.access_level_id === "Super User") {
            return User.findById(params.userId)
            .then((user,err) => {
                if (err) {
                    return false
                } else if(user.isActive === true) {
                    user.isActive = false
                } else {
                    user.isActive = true
                }
                return user.save()
            })
            .then((updatedUser, err) => {
                return (err) ? false : true
            })
        }
    })
}