const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    age: {
        type: String,
        required: [true, 'Age is required']
    },
    birth_date: {
        type: String,
        required: [true, 'Birth date is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    job_title: {
        type: String,
        required: [true, 'Job title is required']
    },
    access_level_id: {
        type: String,
        required: [true, 'Access Level Id is required']
    },
    isActive: {
        type: Boolean,
        default: true
    }
})

module.exports = mongoose.model('user', userSchema)